#include <iostream>
#include <math.h>
using namespace std;


int main(){

    float a,b,c;
    float discriminante,x1,x2,Vx,Vy,vertice;

    cout << "Bienvenido a la calculadora de caracteristicas de una funcion cuadratica." << endl;
    cout << "Ingrese el valor de a:" << endl;
    cin>>a;
    cout << "Ingrese el valor de b:" << endl;
    cin >> b;
    cout << "Ingrese el valor de c:" << endl;
    cin>>c;

    
    if (a == 0){
        cout << "El valor ingresado para a no corresponde a una ecuacion cuadratica" << endl;
    }
    else if (a > 0){
        cout << "La grafica de la funcion es Convexa." << endl;
    }
    else{
        cout << "La grafica de la funcion es Concava." << endl;
    }


    discriminante = (b*b)-(4*a*c);
    x1 = (-b+sqrt(discriminante))/(2*a);
    x2 = (-b-sqrt(discriminante))/(2*a);
    Vx = (-b)/(2*a);
    Vy = a*Vx*Vx + b*Vx + c;


    cout << "La interseccion con el eje Y es: " << c << endl;
    if (discriminante > 0){
        cout << "Tiene dos raices reales distintas que son: " << x1 << " y " << x2 << endl;
    }
    else if (discriminante == 0){
        cout << "Tiene dos raices iguales que son: " << x1 << " y " << x2 << endl;
    }
    else{
        cout << "La Ecuacion no tiene soluciones Reales" << endl;
    }
    
    cout << "El discriminante es: " << discriminante << endl;
    cout << "El vertice es: ("<< Vx <<","<< Vy <<")" << endl;

    return 0;
}
